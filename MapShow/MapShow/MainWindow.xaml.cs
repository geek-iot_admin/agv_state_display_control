﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace MapShow
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        List<Ellipse> ellipses = new List<Ellipse>();

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            //不进行跨线程安全检查

            for (int i = 0; i < 100; i++)
            {
                var el = new Ellipse();
                el.Height = 10;
                el.Width = 10;
                el.Margin = new Thickness(i * 5, i * 5, 0, 0);
                el.StrokeThickness = 2;
                el.Fill = Brushes.Yellow;
                el.Stroke = Brushes.Gray;
                el.PreviewMouseMove += El_PreviewMouseMove;
                ellipses.Add(el);
                canv_1.Children.Add(ellipses[i]);

            }
            Thread thread = new Thread(FuncRun);
            thread.IsBackground = true;
            thread.Start();
        }

        private void El_PreviewMouseMove(object sender, MouseEventArgs e)
        {
            var el = sender as Ellipse;
            el.Fill = Brushes.Blue;
        }

        private void FuncRun()
        {

            while (true)
            {
                for (int i = 0; i < 100; i++)
                {
                    this.Dispatcher.Invoke(new Action(() =>
                    {
                        ellipses[i].Fill = Brushes.Red;
                    }));
                    Thread.Sleep(50);
                    this.Dispatcher.Invoke(new Action(() =>
                    {
                        ellipses[i].Fill = Brushes.Yellow;
                    }));
                    Thread.Sleep(50);

                }
                Thread.Sleep(1000);
            }
        }
    }
}
